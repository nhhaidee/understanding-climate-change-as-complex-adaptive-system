# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 16:03:23 2016
@author: Hai Nguyen Hoang
Course: Complex Adaptive Systems
Model: Climate Change
Experiment 1: Verify the impact of ice/snow cover to the earth's temperature.
"""

## PypeR ypeR (PYthon-piPE-R, originally names as Rinpy - R in Python) allows people to use R in Python through PIPE.

from pyper import *
import matplotlib.pyplot as plt
import pandas as pd
r = R(RCMD="C:\\Program Files\\R\\R-3.3.1\\bin\\x64\\R")
r('library(RNetLogo)')
r('nl.path <- "C:\\Program Files\\NetLogo 5.3.1\\app"')
r('NLStart(nl.path, gui=TRUE)')
r('model.path <- "/models/Sample Models/Earth Science/CAS_ClimateChange_FinalProject.nlogo"')
r('NLLoadModel(paste(nl.path,model.path,sep=""))')
########################################## Main Code ################################################
def setup():
   r('NLCommand("setup")')
   r('NLCommand("set sun-brightness 1.4")')
   r('NLCommand("set landtype1-albedo 0.2")')
   r('NLCommand("set landtype2-albedo 0.3")')
   r('NLCommand("set landtype3-albedo 0.4")')
   r('NLCommand("set landtype4-albedo 0.8")')
   r('NLCommand("set day-night? TRUE")')
   r('NLCommand("set ice-albedo-feedback? FALSE")')
setup()
r('NLDoCommand(4,"add-cloud")')
r('NLDoCommand(7,"add-CO2")') ## Amount of CO2 7 * 25 = 175
r('NLDoCommand(7,"add-CH4")') ## Amount of CH4 7 * 8 = 56
r('EarthTemperature1 <- NLDoReport(50000, "go", c("temperature"),as.data.frame= TRUE, df.col.names = c("temperature1"))')
## Data Frame
df1 = r.EarthTemperature1
## Extend ice/snow cover
r('NLCommand("set size-landtype2 20")')
setup()
r('NLDoCommand(4,"add-cloud")')
r('NLDoCommand(7,"add-CO2")') ## Amount of CO2 7 * 25 = 175
r('NLDoCommand(7,"add-CH4")') ## Amount of CH4 7 * 8 = 56
r('EarthTemperature2 <- NLDoReport(50000, "go", c("temperature"), as.data.frame= TRUE, df.col.names = c("temperature2"))')
## Data Frame
df2 = r.EarthTemperature2
plt.xlabel("Time Step")
plt.ylabel("Earth Temperature")
## Plot Data Frames of two cases
plt.figure(figsize=(10,5))
plt.plot(df1,'r',label ='Normal snow cover')
plt.plot(df2,'b',label ='Extented snow cover')
plt.legend(loc='lower right')
r('NLQuit()')