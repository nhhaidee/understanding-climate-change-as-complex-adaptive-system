# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 16:03:23 2016
@author: Hai Nguyen Hoang
Course: Complex Adaptive Systems
Model: Climate Change
Experiment 2: Verify ice/albedo feedback.
"""

## PypeR ypeR (PYthon-piPE-R, originally names as Rinpy - R in Python) allows people to use R in Python through PIPE.

from pyper import *
import matplotlib.pyplot as plt1
import matplotlib.pyplot as plt2
import matplotlib.pyplot as plt3
y1, y2 = 95, 115
earthTemperature = []
greenHouseGases = []
timeStep = []
patchColor = []
i = 0
r = R(RCMD="C:\\Program Files\\R\\R-3.3.1\\bin\\x64\\R")
r('library(RNetLogo)')
r('nl.path <- "C:\\Program Files\\NetLogo 5.3.1\\app"')
r('NLStart(nl.path, gui=TRUE)')
r('model.path <- "/models/Sample Models/Earth Science/CAS_ClimateChange_FinalProject.nlogo"')
r('NLLoadModel(paste(nl.path,model.path,sep=""))')
######################################### Main Code ############################################
r('NLCommand("setup")')
r('NLCommand("set sun-brightness 1")')
r('NLCommand("set landtype1-albedo 0.2")')
r('NLCommand("set landtype2-albedo 0.3")')
r('NLCommand("set landtype3-albedo 0.4")')
r('NLCommand("set landtype4-albedo 0.8")')
r('NLCommand("set day-night? FALSE")')
r('NLCommand("set ice-albedo-feedback? TRUE")')
r('NLDoCommand(3,"add-cloud")')
r('NLDoCommand(5,"add-CO2")') ## Amount of CO2 5 * 25 = 125
r('NLDoCommand(7,"add-CH4")') ## Amount of CH4 7 * 8 = 56
r('NLCommand("set ice-melt-threshold 25")')
r('NLDoCommand(4,"add-factory")')
r('NLDoCommand(4,"add-tree")')
def currentTemperature():
     r('temp1 <- NLReport("temperature")')
     temp1 = r.temp1
     return temp1   
def currentTicks():
     r('temp2 <- NLReport("ticks")')
     temp2 = r.temp2
     return temp2
def currentGreenHouseGases():
     r('temp3 <- NLReport("count CO2s + count CH4s")')
     temp3 = r.temp3
     return temp3
## Run the test in 50000 ticks
while (currentTicks() < 50000):
    r('NLCommand("go")')
    earthTemperature.append(currentTemperature())
    greenHouseGases.append(currentGreenHouseGases())
    timeStep.append(currentTicks())
    r('pcolorIce <- NLGetPatches(c("pcolor"), "patches with [pxcor = -20 and pycor = 0]",as.data.frame=FALSE, patches.by.row=TRUE)')
    patchColor.append(r.pcolorIce)
    if (currentTemperature() > 30 and i == 0 ):
        r('NLDoCommand(9,"add-cloud")')
        i = i + 1 ## Never run the if statement again
plt1.figure(figsize=(10,5))
plt1.xlabel("Time Steps")
plt1.ylabel("GreenHouse Gases")
plt1.plot(timeStep,greenHouseGases)
plt2.figure(figsize=(10,5))
plt2.xlabel("Time Steps")
plt2.ylabel("Earth Temperature")
plt2.plot(timeStep,earthTemperature)
plt3.figure(figsize=(10,5))
plt3.ylim([y1, y2])
plt3.xlabel("Time Steps")
plt3.ylabel("Patch Color of Ice Area")
plt3.plot(timeStep,patchColor,'r')
r('NLQuit()')