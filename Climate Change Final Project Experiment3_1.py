# -*- coding: utf-8 -*-
"""
Created on Tue Nov 22 16:03:23 2016
@author: Hai Nguyen Hoang
Course: Complex Adaptive Systems
Model: Climate Change
Experiment 3.1: Verify human activities impact to Earth Climate with ice/albedo feedback is FALSE.
"""

## PypeR ypeR (PYthon-piPE-R, originally names as Rinpy - R in Python) allows people to use R in Python through PIPE.

from pyper import *
import matplotlib.pyplot as plt1
import matplotlib.pyplot as plt2
earthTemperature1 = []
greenHouseGases1 = []
earthTemperature2 = []
greenHouseGases2 = []
timeStep = []
i = 0
r = R(RCMD="C:\\Program Files\\R\\R-3.3.1\\bin\\x64\\R")
r('library(RNetLogo)')
r('nl.path <- "C:\\Program Files\\NetLogo 5.3.1\\app"')
r('NLStart(nl.path, gui=TRUE)')
r('model.path <- "/models/Sample Models/Earth Science/CAS_ClimateChange_FinalProject.nlogo"')
r('NLLoadModel(paste(nl.path,model.path,sep=""))')
######################################### Main Code ############################################
def setup():
   r('NLCommand("setup")')
   r('NLCommand("set sun-brightness 1.4")')
   r('NLCommand("set landtype1-albedo 0.2")')
   r('NLCommand("set landtype2-albedo 0.3")')
   r('NLCommand("set landtype3-albedo 0.4")')
   r('NLCommand("set landtype4-albedo 0.8")')
   r('NLCommand("set landtype4-albedo 0.8")')
   r('NLCommand("set day-night? TRUE")')
   r('NLCommand("set ice-albedo-feedback? FALSE")')
   r('NLDoCommand(3,"add-cloud")')
   r('NLDoCommand(5,"add-CO2")') ## Amount of CO2 5 * 25 = 125
   r('NLDoCommand(7,"add-CH4")') ## Amount of CH4 7 * 8 = 56
   r('NLCommand("set ice-melt-threshold 25")')
   r('NLDoCommand(4,"add-factory")')
   r('NLDoCommand(3,"add-tree")')
def currentTemperature():
     r('temp1 <- NLReport("temperature")')
     temp1 = r.temp1
     return temp1   
def currentTicks():
     r('temp2 <- NLReport("ticks")')
     temp2 = r.temp2
     return temp2
def currentGreenHouseGases():
     r('temp3 <- NLReport("count CO2s + count CH4s")')
     temp3 = r.temp3
     return temp3
## Run the test with regular human activities
setup()
r('cloud1 <- NLReport("count clouds")')
print (r.cloud1)
while (currentTicks() < 50000):
    r('NLCommand("go")')
    earthTemperature1.append(currentTemperature())
    greenHouseGases1.append(currentGreenHouseGases())
    timeStep.append(currentTicks())
## Run the test with more human activities
setup()
r('cloud2 <- NLReport("count clouds")')
print (r.cloud2)
while (currentTicks() < 50000):
    r('NLCommand("go")')
    earthTemperature2.append(currentTemperature())
    greenHouseGases2.append(currentGreenHouseGases())
    if (currentTicks() == 25000):
        r('NLDoCommand(1,"add-factory")')
plt1.figure(figsize=(10,5))
plt1.xlabel("Time Steps")
plt1.ylabel("GreenHouse Gases")
plt1.plot(timeStep,greenHouseGases1,'b',label ='Normal human activites')
plt1.plot(timeStep,greenHouseGases2,'r',label ='More human activites')
plt1.legend(loc='lower right')
plt2.figure(figsize=(10,5))
plt2.xlabel("Time Steps")
plt2.ylabel("Earth Temperature")
plt2.plot(timeStep,earthTemperature1,'b',label ='Normal human activites')
plt2.plot(timeStep,earthTemperature2,'r',label ='More human activites')
plt2.legend(loc='lower right')
r('NLQuit()')